﻿/* Esta es la parte grafica, este es el que debe de imprimir a consola */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Ajedrez.GameObjects.Pieces;

namespace Ajedrez.GameObjects
{
    public class Game
    {
        private const int MaxRows = 8;
        private const int MaxColumns = 8;
        private readonly Board _board;
        private readonly Dictionary<int, string> _columnsDictionary;

        public Game()
        {
            _board = new Board();
            _columnsDictionary = _board.ColumnLetterDictionary;
        }

        private const string SquarePlaceHolder = "___";

        private static string GetPieceName(Piece piece)
        {
            var abbreviation = piece.Abbreviation;
            var color = (piece.Color == Color.Black) ? "N" : "B";
            return abbreviation + color;
        }

        private void DrawBoard()
        {
            var rowDelimiter = "";
            for (var i = 0; i < (MaxColumns * 4); i++)
            {
                rowDelimiter += "-";
            }            
            
            for (var row = MaxRows; row >= 1; row--)
            {
                Console.Write("{0}  ", row);
                for (var column = 1; column <= MaxColumns; column++)
                {
                    var square = _board.GetSquare(row, column);
                    Piece piece = null;

                    if (square.HeldPiece != null)
                        piece = square.HeldPiece;

                    var render = SquarePlaceHolder;

                    if (piece != null)
                        render = GetPieceName(piece);

                    Console.Write("{0}|", render);
                }

                Console.WriteLine("");
                Console.Write("   ");
                Console.WriteLine(rowDelimiter);
            }
            Console.Write("   ");
            Console.Write(" A   B   C   D   E   F   G   H");
            Console.WriteLine("");
        }

        private void SquareStringToRowColumn(out int row, out int column, string square)
        {
            var firstChar = square[0].ToString(CultureInfo.InvariantCulture);
            var secondChar = square[1].ToString(CultureInfo.InvariantCulture);

            if (int.TryParse(firstChar, out row))
                column = _columnsDictionary.FirstOrDefault(pair => pair.Value == secondChar.ToUpper()).Key;
            else
            {
                row = int.Parse(secondChar);
                column = _columnsDictionary.FirstOrDefault(pair => pair.Value == firstChar.ToUpper()).Key;
            }
        }

        private string GetSquareFromRowColumn(int row, int column)
        {
            return _columnsDictionary[column] + row;
        }

        public void ExecuteGame()
        {
            do
            {
                DrawBoard();
                Console.WriteLine("Turno del jugador " + _board.CurrentTurn);
                Console.WriteLine("Seleccione la pieza a mover, o presione 'x' para salir");
                string originString = Console.ReadLine();

                if (originString != null) 
                    originString = originString.Replace(" ",string.Empty);
                if (originString != null && originString.ToLower() == "x") 
                    break;
                if (string.IsNullOrEmpty(originString) || originString.Length < 2)
                {
                    Console.WriteLine("Intenta de nuevo: ");
                    PressAnyKeyToContinue();
                    continue;
                }
                
                int originRow;
                int originCol;
                SquareStringToRowColumn(out originRow, out originCol, originString);
                var originSquare = _board.GetSquare(originRow, originCol);
                    
                var posibilitiesList = _board.MovementPosibilitiesList(originSquare);
                if (posibilitiesList == null)
                {
                    Console.WriteLine("La Casilla es vacia!");
                    PressAnyKeyToContinue();
                    continue;
                } 
                Console.WriteLine("Has seleccionado: " + originSquare.HeldPiece.Name);
                Console.WriteLine("Estos son sus posibles movimientos: ");
                foreach (var posibleMove in posibilitiesList)
                    Console.Write("♦" + GetSquareFromRowColumn(posibleMove.Row, posibleMove.Column) + " ");
                if (!posibilitiesList.Any())
                {
                    Console.WriteLine("No hay movimientos Disponibles");
                    continue;
                }
                Console.WriteLine("Seleccione lugar de destino: ");
                string destinationString = Console.ReadLine();
                int destinationRow;
                int destinationCol;
                if (destinationString != null) destinationString = destinationString.Replace(" ", string.Empty);
                if (string.IsNullOrEmpty(destinationString) || destinationString.Length < 2)
                {
                    Console.WriteLine("Intenta de nuevo");
                    PressAnyKeyToContinue();
                    continue;
                }
                SquareStringToRowColumn(out destinationRow, out destinationCol, destinationString);
                var destinationSquare = _board.GetSquare(destinationRow, destinationCol);
                if (!posibilitiesList.Contains(destinationSquare))
                {
                    Console.WriteLine("Movimiento invalido");
                    PressAnyKeyToContinue();
                    continue;
                }
                var response = _board.MovePiece(originSquare, destinationSquare);
                Console.WriteLine(UpperCaseSplit(response) + "!");
                if (response != Output.JaqueMate) continue;
                DrawBoard();
                Console.WriteLine("El ganador es: El jugador " + _board.NextTurn());
                break;
            } while (true);
            Console.WriteLine("Gracias por jugar!");
            PressAnyKeyToContinue();
        }

        private static string UpperCaseSplit(Output response)
        {
            var output = "";

            foreach (char letter in response.ToString())
            {
                if (Char.IsUpper(letter) && output.Length > 0)
                    output += " " + letter;
                else
                    output += letter;
            }
            return output;
        }

        private static void PressAnyKeyToContinue()
        {
            Console.WriteLine("Presiona cualquier tecla para continuar.");
            Console.ReadKey();
        }
    }
}

