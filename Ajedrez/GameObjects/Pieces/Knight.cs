using System.Collections.Generic;
using System.Linq;

namespace Ajedrez.GameObjects.Pieces
{
    class Knight : Piece
    {
        public override string Abbreviation
        {
            get { return "�C"; }
        }

        public override string Name
        {
            get { return "Caballero"; }
        }

        public override List<Square> GetMovementRange(Square square, List<Square> squares)
        {
            var output = new List<Square>();
            for (var i = 2; i >= -2; i--)
            {
                if (i == 0) continue;
                var j = (i == 2 || i == -2) ? 1 : 2;
                output.AddRange(squares.Where(tempSquare => tempSquare.Row == square.Row + i
                    && tempSquare.Column == square.Column + j
                    && (tempSquare.HeldPiece == null || tempSquare.HeldPiece.Color != square.HeldPiece.Color)));
                output.AddRange(squares.Where(tempSquare => tempSquare.Row == square.Row + i
                    && tempSquare.Column == square.Column - j
                    && (tempSquare.HeldPiece == null || tempSquare.HeldPiece.Color != square.HeldPiece.Color)));
            }
            return output;
        }
    }
}