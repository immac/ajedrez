﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ajedrez.GameObjects.Pieces
{
    class Pawn : Piece
    {
        public override string Abbreviation
        {
            get { return "▲P"; }
        }

        public override string Name
        {
            get { return "Peon"; }
        }

        public override List<Square> GetMovementRange(Square square, List<Square> squares)
        {
            Direction direction;
            int range;
            var returnValue = new List<Square>();
            switch (square.HeldPiece.Color)
            {
                case Color.White:
                    range = square.Row == 2 ? 2 : 1;
                    direction = Direction.Up;
                    break;
                case Color.Black:
                    range = square.Row == 7 ? 2 : 1;
                    direction = Direction.Down;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            returnValue.AddRange(GetRangeForDirection(square, square, direction, squares, range));
            returnValue.AddRange(PeonCaptureCheck(square, squares));
            return returnValue;
        }

        private List<Square> PeonCaptureCheck(Square square, List<Square> squares)
        {
            var output = new List<Square>();
            if (square.HeldPiece.Color == Color.White)
            {
                var enemyUR = GetSquare(square.Row + 1, square.Column + 1, squares);
                var enemyUL = GetSquare(square.Row + 1, square.Column - 1,squares);
                if (IsCapturable(square, enemyUL)) output.Add(enemyUL);
                if (IsCapturable(square, enemyUR)) output.Add(enemyUR);
            }
            else
            {
                var enemyDR = GetSquare(square.Row - 1, square.Column + 1,squares);
                var enemyDL = GetSquare(square.Row - 1, square.Column - 1,squares);
                if (IsCapturable(square, enemyDL)) output.Add(enemyDL);
                if (IsCapturable(square, enemyDR)) output.Add(enemyDR);
            }
            return output;
        }
        private static bool IsCapturable(Square square, Square attackedSquare)
        {
            return attackedSquare != null && attackedSquare.HeldPiece != null && attackedSquare.HeldPiece.Color != square.HeldPiece.Color;
        }


        public Square GetSquare(int row, int col, List<Square> _squares)
        {
            var squares = _squares.AsEnumerable().Where(square => square.Column == col && square.Row == row).ToArray();
            return squares.Any() ? squares[0] : null;
        }
    }
}