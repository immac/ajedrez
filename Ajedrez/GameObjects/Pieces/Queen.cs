﻿using System.Collections.Generic;

namespace Ajedrez.GameObjects.Pieces
{
    class Queen : Piece
    {
        public override string Abbreviation
        {
            get { return "♀D"; }
        }

        public override string Name
        {
            get { return "Reina"; }
        }

        public override List<Square> GetMovementRange(Square square, List<Square> squares)
        {
            var output = new List<Square>();
            output.AddRange(GetRangeForDirection(square, square, Direction.Up, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.Down, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.Left, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.Right, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.LeftUp, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.LeftDown, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.RightDown, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.RightUp, squares));
            return output;
        }
    }
}