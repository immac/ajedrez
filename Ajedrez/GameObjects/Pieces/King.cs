﻿using System.Collections.Generic;

namespace Ajedrez.GameObjects.Pieces
{
    public class King : Piece
    {
        public override string Abbreviation
        {
            get { return "♂R"; }
        }

        public override string Name
        {
            get { return "Rey"; }
        }

        public override List<Square> GetMovementRange(Square square, List<Square> squares)
        {
            var output = new List<Square>();

            output.AddRange(GetRangeForDirection(square, square, Direction.RightUp, squares, 1));
            output.AddRange(GetRangeForDirection(square, square, Direction.Right, squares, 1));
            output.AddRange(GetRangeForDirection(square, square, Direction.RightDown, squares, 1));
            output.AddRange(GetRangeForDirection(square, square, Direction.Down, squares, 1));
            output.AddRange(GetRangeForDirection(square, square, Direction.LeftDown, squares, 1));
            output.AddRange(GetRangeForDirection(square, square, Direction.Left, squares, 1));
            output.AddRange(GetRangeForDirection(square, square, Direction.LeftUp, squares, 1));

            return output;
        }
  
    }
}