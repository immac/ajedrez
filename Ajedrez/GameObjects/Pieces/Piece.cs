﻿using System.Collections.Generic;
using System.Linq;

namespace Ajedrez.GameObjects.Pieces
{
    public enum Color { White, Black };

    public abstract class Piece
    {
        public int Id { set; get; }
        public Color Color { set; get; }
        public abstract string Abbreviation { get; }
        public abstract string Name { get; }
    

        public abstract List<Square> GetMovementRange(Square square, List<Square> squares);

        private static Square NextSquare(Square origin, Square next, Direction direction, IEnumerable<Square> squares)
        {
            var rowOffset = RowOffset(direction);
            var colOffset = ColOffset(direction);
            return squares.FirstOrDefault(square => square.Row == next.Row + rowOffset
                                            && square.Column == next.Column + colOffset
                                            && (square.HeldPiece == null
                                                || (square.HeldPiece.Color != origin.HeldPiece.Color
                                                && !(origin.HeldPiece is Pawn)))
                                           );
        }

        // ReSharper disable once ParameterTypeCanBeEnumerable.Local
        protected IEnumerable<Square> GetRangeForDirection(Square origin, Square next, Direction direction,IList<Square> squares, int maximumRange = 9)
        {
            var output = new List<Square>();
            if (maximumRange == 0) return output;
            next = NextSquare(origin, next, direction, squares);
            if (next == null) return output;
            output.Add(next);
            if (this is Pawn && next.HeldPiece != null)
                return output;
            maximumRange--;
            if (next.HeldPiece == null)
                output.AddRange(GetRangeForDirection(origin, next, direction, squares, maximumRange));
            return output;
        }
        private static int RowOffset(Direction direction)
        {
            var rowOffset = 0;
            switch (direction)
            {
                case Direction.LeftUp:
                case Direction.RightUp:
                case Direction.Up:
                    rowOffset = 1;
                    break;
                case Direction.RightDown:
                case Direction.LeftDown:
                case Direction.Down:
                    rowOffset = -1;
                    break;
            }
            return rowOffset;
        }

        private static int ColOffset(Direction direction)
        {
            var colOffset = 0;
            switch (direction)
            {
                case Direction.LeftDown:
                case Direction.LeftUp:
                case Direction.Left:
                    colOffset = 1;
                    break;
                case Direction.RightUp:
                case Direction.RightDown:
                case Direction.Right:
                    colOffset = -1;
                    break;
            }
            return colOffset;
        }
    }
}
