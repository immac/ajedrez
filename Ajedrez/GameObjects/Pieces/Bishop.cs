﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace Ajedrez.GameObjects.Pieces
{
    public class Bishop : Piece
    {
        public override string Abbreviation
        {
            get { return "§A"; }
        }

        public override string Name
        {
            get { return "Alfil"; }
        }

        public override List<Square> GetMovementRange(Square square, List<Square> squares)
        {
            var output = new List<Square>();

            output.AddRange(GetRangeForDirection(square, square, Direction.LeftUp, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.LeftDown, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.RightDown, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.RightUp, squares));

            return output;
        }
    }
}