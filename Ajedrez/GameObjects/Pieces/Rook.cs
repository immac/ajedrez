﻿using System.Collections.Generic;

namespace Ajedrez.GameObjects.Pieces
{
    public class Rook : Piece
    {
        public override string Abbreviation
        {
            get { return "☼T"; }
        }

        public override string Name
        {
            get { return "Torre"; }
        }

        public override List<Square> GetMovementRange(Square square, List<Square> squares)
        {
            var output = new List<Square>();
            output.AddRange(GetRangeForDirection(square, square, Direction.Up, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.Down, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.Left, squares));
            output.AddRange(GetRangeForDirection(square, square, Direction.Right, squares));
            return output;
        }
    }
}
