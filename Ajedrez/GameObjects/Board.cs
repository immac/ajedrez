﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ajedrez.GameObjects.Factories;
using Ajedrez.GameObjects.Pieces;

namespace Ajedrez.GameObjects
{
    using PieceInit = Tuple<int, int, Color, PieceType>;
    public enum Output
    {
        Exito,Jaque,AutoJaque,
        NoEsTuTurno,
        JaqueMate,
        Estancamiento
    }

    public enum Direction
    {
        Up,Down,Left,Right,RightUp,LeftUp,RightDown,LeftDown    
    }
    enum PieceType
    {
        Pawn,Rook,Knight,Bishop,Queen,King
    }

    public enum PlayerColor
    {
        Blanco,Negro
    }
        public class Board
        {
            private readonly List<PieceInit> _initialValueSets =
                new List<PieceInit>();
            
            private readonly Dictionary<int, string> _columnLetterDictionary = new Dictionary<int, string>
            {
                {1,"A"},{2,"B"},{3,"C"},{4,"D"},{5,"E"},{6,"F"},{7,"G"},{8,"H"}
            };

            private readonly Dictionary<int, string> _piecesDictionary = new Dictionary<int, string>
            {
                {0, "Peon"},{1, "Torre"},{2, "Caballero"},{3, "Alfil"},{4, "Reina"},{5, "Rey"}
            };
            public Dictionary<int, string> ColumnLetterDictionary{get { return _columnLetterDictionary; }}
            public Dictionary<int, string> PiecesDictionary{get { return _piecesDictionary; }}
            
            private const int MaxRows = 8;
            private const int MaxColumns = 8;
            private int _pieceCounter;

            
            private readonly List<Piece> _pieces;
            private readonly List<Square> _squares;

            public Board()
            {
                _pieces = new List<Piece>();
                _squares = new List<Square>();
                CurrentTurn = PlayerColor.Blanco;      
          
                InitializePosition();
                CreateSquares();
                FillSquares();
            }

            private void InitializePosition()
            {
                for (var columna = 1; columna <= MaxColumns; columna++)
                    _initialValueSets.Add(new PieceInit(2, columna, Color.White, PieceType.Pawn));
                for (var columna = 1; columna <= MaxColumns; columna++)
                    _initialValueSets.Add(new PieceInit(7, columna, Color.Black, PieceType.Pawn));

                _initialValueSets.Add(new PieceInit(1, 1, Color.White, PieceType.Rook));
                _initialValueSets.Add(new PieceInit(1, 2, Color.White, PieceType.Knight));
                _initialValueSets.Add(new PieceInit(1, 3, Color.White, PieceType.Bishop));
                _initialValueSets.Add(new PieceInit(1, 4, Color.White, PieceType.Queen));
                _initialValueSets.Add(new PieceInit(1, 5, Color.White, PieceType.King));
                _initialValueSets.Add(new PieceInit(1, 6, Color.White, PieceType.Bishop));
                _initialValueSets.Add(new PieceInit(1, 7, Color.White, PieceType.Knight));
                _initialValueSets.Add(new PieceInit(1, 8, Color.White, PieceType.Rook));

                _initialValueSets.Add(new PieceInit(8, 1, Color.Black, PieceType.Rook));
                _initialValueSets.Add(new PieceInit(8, 2, Color.Black, PieceType.Knight));
                _initialValueSets.Add(new PieceInit(8, 3, Color.Black, PieceType.Bishop));
                _initialValueSets.Add(new PieceInit(8, 4, Color.Black, PieceType.Queen));
                _initialValueSets.Add(new PieceInit(8, 5, Color.Black, PieceType.King));
                _initialValueSets.Add(new PieceInit(8, 6, Color.Black, PieceType.Bishop));
                _initialValueSets.Add(new PieceInit(8, 7, Color.Black, PieceType.Knight));
                _initialValueSets.Add(new PieceInit(8, 8, Color.Black, PieceType.Rook));
            }
            private void CreateSquares()
            {
                var squareColor = SquareColor.Black;
                for (var col = 1; col <= MaxColumns; col++)
                {
                    for (var row = 1; row <= MaxRows; row++)
                    {
                        _squares.Add(SquareFactory.NewSquare(squareColor, col, row));
                        squareColor = NextColor(squareColor);
                    }
                }
            }

            private static SquareColor NextColor(SquareColor tempSquareColor)
            {
                return tempSquareColor == SquareColor.Black ? SquareColor.White : SquareColor.Black;
            }

            private void FillSquares()
            {
                foreach (var initValSet in _initialValueSets)
                {
                    GetSquare(initValSet.Item1, initValSet.Item2).HeldPiece = 
                        CrearPieza(initValSet.Item4,initValSet.Item3);
                }
            }

            private  Piece CrearPieza(PieceType type,Color color)
            {   
                var newPiece = PieceFactory.MakePiece(type, color, _pieceCounter++);
                _pieces.Add(newPiece);
                return newPiece;
            }
            
            public Square GetSquare(int row, int col)
            {
                var squares = _squares.AsEnumerable().Where(square => square.Column == col && square.Row == row).ToArray();
                return squares.Any() ? squares[0] : null;
            }
            public Square GetSquare(int id)
            {
                return _squares.Find(square => square.Id == id);
            }

            private Piece GetPieceInSquare(int idCasilla)
            {
                var piece = _squares.First(casilla => casilla.HeldPiece != null && casilla.Id == idCasilla).HeldPiece;
                return piece;
            }
            public Piece GetPieceInSquare(int fila, int columna)
            {
                return GetPieceInSquare(GetSquare(fila,columna).Id);
            }

            public List<Square> MovementPosibilitiesList(Square square)
            {
                var heldPiece = square.HeldPiece;
                return heldPiece == null ? null : PieceMovement(square);
            }

            private List<Square> PieceMovement(Square square)
            {
                return square.HeldPiece.GetMovementRange(square, _squares);
            }

            public Output MovePiece(Square originSquare,Square destinationSquare,bool test = false)
            {
                var originPiece = originSquare.HeldPiece;
                
                var destinationPiece = destinationSquare.HeldPiece;
                
                if (originPiece.Color != (Color) CurrentTurn) return Output.NoEsTuTurno;

                destinationSquare.HeldPiece = originSquare.HeldPiece;
                originSquare.HeldPiece = null;
    
                if (CheckCheck(originPiece.Color))
                {
                    originSquare.HeldPiece = originPiece;
                    destinationSquare.HeldPiece = destinationPiece;
                    return Output.AutoJaque;
                }
                if (test)
                {
                    originSquare.HeldPiece = originPiece;
                    destinationSquare.HeldPiece = destinationPiece;
                    return Output.Exito;
                }

                
                NextTurn();
                if (originPiece is Pawn)
                    destinationSquare.HeldPiece = CheckPromotion(destinationSquare);
                var output = CheckCheckMate(OponentColor(originPiece));
                return output != Output.Exito
                    ? output
                    : (CheckCheck(OponentColor(originPiece)) ? Output.Jaque : Output.Exito);
            }

            private Piece CheckPromotion(Square square)
            {
                var piece = square.HeldPiece;
                var replacement = PieceFactory.MakePiece(PieceType.Queen, piece.Color, piece.Id);
                if (piece.Color == Color.White && square.Row == 8)
                    return replacement;
                if (piece.Color == Color.Black && square.Row == 1)
                    return replacement;
                return piece;
            }

            private static Color OponentColor(Piece originPiece)
            {
                return originPiece.Color == Color.White ? Color.Black : Color.White;
            }

            private Output CheckCheckMate(Color color)
            {
                var squaresOfColor = _squares.Where(square => square.HeldPiece != null 
                    && square.HeldPiece.Color == color);
                foreach (var square in squaresOfColor)
                {
                    var moves = MovementPosibilitiesList(square);
                    foreach (Square movimiento in moves)
                    {
                        if (MovePiece(square, movimiento, true) != Output.AutoJaque) return Output.Exito;
                    }
                }
                return CheckCheck(color) ? Output.JaqueMate : Output.Estancamiento;
            }
            
            public PlayerColor NextTurn()
            {
                CurrentTurn = CurrentTurn == PlayerColor.Blanco ? PlayerColor.Negro : PlayerColor.Blanco;
                return CurrentTurn;
            }

            public PlayerColor CurrentTurn { get; set; }

            private bool CheckCheck(Color color)
            {
                var king = GetKing(color);
                var endangeredSquares = EndangeredSquares(king);
                var isKingChecked = endangeredSquares.Contains(king);
                return isKingChecked;
            }

            private Square GetKing(Color color)
            {      
                return _squares.FirstOrDefault(
                    square => square.HeldPiece is King
                    && square.HeldPiece.Color == color);
            }

            private List<Square> EndangeredSquares(Square squareKing)
            {
                var oponentColor = OponentColor(squareKing.HeldPiece);
                var enemyPieces = _squares.FindAll(square => square.HeldPiece != null 
                        && square.HeldPiece.Color == oponentColor);
                var endangeredSquares = new List<Square>();
                foreach (var enemyPiece in enemyPieces)
                {
                    endangeredSquares.AddRange(MovementPosibilitiesList(enemyPiece));
                }
                return endangeredSquares;
            }

            public IEnumerable<Square> SelectPiece(int row,int col)
            {
                return MovementPosibilitiesList(GetSquare(row, col));
            }


        }
    }
