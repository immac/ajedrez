﻿using System;
using Ajedrez.GameObjects.Pieces;

namespace Ajedrez.GameObjects.Factories
{
    static class PieceFactory
    {

        public static Piece MakePiece(PieceType type, Color color, int idPiece)
        {
            switch (type)
            {
                case PieceType.Pawn:
                    return new Pawn
                    {
                        Color = color,
                        Id = idPiece,
                    };
                case PieceType.Rook:
                    return new Rook
                    {
                        Color = color,
                        Id = idPiece,
                    };
                case PieceType.Knight:
                    return new Knight
                    {
                        Color = color,
                        Id = idPiece,
                    };
                case PieceType.Bishop:
                    return new Bishop
                    {
                        Color = color,
                        Id = idPiece,
                    };
                case PieceType.Queen:
                    return new Queen
                    {
                        Color = color,
                        Id = idPiece,
                    };
                case PieceType.King:
                    return new King
                    {
                        Color = color,
                        Id = idPiece,
                    };
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
