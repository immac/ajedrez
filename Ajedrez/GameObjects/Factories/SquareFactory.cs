﻿namespace Ajedrez.GameObjects.Factories
{
    static class SquareFactory
    {
        public static Square NewSquare(SquareColor squareColor, int columna, int fila)
        {
            return new Square
            {
                SquareColor = squareColor,
                Column = columna,
                Row = fila
            };
        }
    }
}
