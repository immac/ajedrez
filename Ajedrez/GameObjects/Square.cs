﻿using Ajedrez.GameObjects.Pieces;

namespace Ajedrez.GameObjects
{
    public enum SquareColor {White,Black}

    public class Square
    {
        public int Id { set; get; }
        public SquareColor SquareColor { set; get; }
        public int Row { set; get; }
        public int Column { set; get; }
        public Piece HeldPiece { get; set; }
    }
}
